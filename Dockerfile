FROM openjdk:17
COPY target/*.jar /webapp.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "webapp.jar"]
